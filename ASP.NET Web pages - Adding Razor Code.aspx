ASP.NET Web Pages - Adding Razor Code

What is Razor ?

	Razor is a Markup Syntax for adding server-based code to web pages
	Razor has the power of traditional ASP.NET markup, but is easier to learn, and easier to use.
	Razor is a server side markup syntax much like ASP and PHP
	Razor supports C# and VB Programming Languages

Adding Razor Code

	<!-- Without Razor Code -->
	<html lang = "en">
	<head>
		<meta charset="utf-8"/>
		<title>Web Pages Demo</title>
	</head>

	<body>
		<h1>Hello Web Pages</h1>
		<p>The time is @DateTime.Now</p>
	</body>
	</html>

<!-- With Razor Code -->
	<html lang = "en">
	<head>
		<meta charset="utf-8"/>
		<title>Web Pages Demo</title>
	</head>

	<body>
		<h1>Hello Web Pages</h1>
		<p>The time is @DateTime.Now</p> // Razor Code
	</body>
	</html>

	The page contains ordinary HTML markup, with one addition: the @ marked Razor code.

	The Razor code does all the work of determining the current time on the server and display it. (You can specify formatting options, or just display the default)

Main Razor Syntax Rules for C#
	Razor code blocks are enclosed in @{...}
	Inline Expressions(variables and funcitons) start with @
	Code statements end with semicolon
	Variables are declared with the var keyword
	Strings are enclosed with quotation marks
	C# code is case sensitive
	C# files have the extension .cshtml


C# Example
	
	<!--Single Statement Block-->
	@{var mymessage = "Hello World";}

	<!--Inline Expression or variable --> 
	<p>The Value of MyMessage is : @myMessage</p>

	<!--MultiLine Expression Variable-->
	@{
	var greeting = "Welcome to our site!";
	var weekDay = DateTime.Now.DayOfWeek;
	var greetingMessage = greeting + "Today is: "+weekday;
	}
	<p>The Greeting is: @greetingMessage</p>


	
Main Razor Syntax Rules for VB
	Razor code blocks are enclosed in @Code ... End Code
	Inline Expressions(variables and funcitons) start with @
	Variables are declared with the Dim keyword
	Strings are enclosed with quotation marks
	C# code is not case sensitive
	C# files have the extension .vbhtml


VB Example
	
	<!--Single Statement Block-->
	@Code dim myMessage = "Hello World" End Code

	<!--Inline Expression or variable --> 
	<p>The Value of MyMessage is : @myMessage</p>

	<!--MultiLine Expression Variable-->
	@Code
	dim greeting = "Welcome to our site!"
	dim weekDay = DateTime.Now.DayOfWeek
	dim greetingMessage = greeting & "Today is: " & weekday
	End Code
	
	<p>The Greeting is: @greetingMessage</p>



	




