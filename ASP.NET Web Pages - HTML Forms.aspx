ASP.NET Web Pages - HTML Forms

Creating An HTML input Page

<html>
<body>
	@{
		if(IsPost)
		{
			string companyname = Request["companyname"];
			string contactname = Request["contactname"];

			<p>You entered: <br/>
			Company Name: @companyname <br/>
			Contact Name: @contactname <br/>
		}
		else
		{
			<form method = "post" action ="">
				Company Name: <br/>
				<input type = "text" name = "Company Name" value="" /><br/>
				Contact Name: <br/>
				<input type = "tet" name = "Contact Name" value = ""/> <br/>
				<input type = "Submit" class = "submit"/>
			</form>
		}
	}
</body>
</html>

Razor Example - Displaying Images
	
	The Example belows shows how to display a selected picture which the user selects from a drop-down list

	@{
		var imagePath = "";
		if(Request["Choice"] != null)
			imagePath = "images/" + Request["Choice"];
	 }

	 <!DOCTYPE html>
	 <html>

	 <body>

	 	<h1>Display Images</h1>

	 	<form method = "post" action = "">
	 		I want to See:
	 		<select name = "Choice"> 
	 			<option value = "Photo1.jpg">Photo 1 </option>
	 			<option value = "Photo2.jpg">Photo 2 </option>
	 			<option value = "Photo3.jpg">Photo 3 </option>
	 		</select>

	 		<input type = "submit" value = "Submit"/>

	 		@if(imagePath != "")
	 		{
	 			<p> 
	 				<img src = "@imagePath" alt = "Sample"/>
	 			</p>
	 		}
	 	</form> 

	 </body>
	 </html>


