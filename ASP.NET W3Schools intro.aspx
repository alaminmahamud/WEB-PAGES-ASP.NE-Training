// ASP.NET is a development framework for building web pages and web sites with HTML, CSS, JS, and server Scripting.

ASP.NET Supports three different development models
1. Web Pages
2. MVC
3. Web Forms

1. Web Pages

	Single Page Model 
	Simplest ASP.NET Model
	Similar to PHP and Classic ASP
	Built in tempates and helper for database, video, graphics and social media and more.

2. MVC
	
	Model View Controller
	MVC separates web applicatiosn into 3 different components:
		Models for data 
		Views for Display
		Controllers for input

3. Web Forms
	
	Event Driven Model
	The Traditional ASP.NET event Driven development model:

	Web pages with added server controls, server events and server code


