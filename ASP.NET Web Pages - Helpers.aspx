ASP.NET Web Pages - Helpers

	Web Helpers greatly simplifies web development and common programming tasks

ASP.NET Helpers
	
	ASP.NET helpers are components that can be accessed by single lines of Razor code.

	You can build your own helpers using Razor syntax stored as .cshtml files, or use built-in ASP.NET helpers.

	You will learn how to use Razor helpers in the next chapters of this tutorial.

	Below is a short description of some useful Razor helpers:
	
The WebGrid Helper
	
	The WebGrid helper simplifies the way to display data:

	Automatically sets up an HTML table to display data
	Supports different options for formatting
	Supports paging (First, next, previous, last) through data
	Supports sorting by clicking on column headings

The Chart Helper
	
The WebMail Helper

The WebImage Helper

Third Party Helpers

