Classic ASP - Active Server Pages


	Active Server Pages (ASP), also known as Classic ASP, was introduced in 1998 as Microsoft's first server side scripting engine.

	ASP is a technology that enables scripts in web pages to be executed by an Internet server.

	ASP pages have the file extension .asp, and are normally written in VBScript.

ASP.NET


	ASP.NET is a new ASP generation. It is not compatible with Classic ASP, but ASP.NET may include Classic ASP.

	ASP.NET pages are compiled, which makes them faster then classic ASP.

	ASP.NET has better language support, a large set of user controls, XML-based components, and integrated user authentication.

	ASP.NET pages have the extension .aspx, and are normally written in VB (Visual Basic) or C# (C sharp). 
	 
	User controls in ASP.NET can be written in different languages, including C++ and Java. 
	 
	When a browser requests an ASP.NET file, the ASP.NET engine reads the file, compiles and executes the scripts in the file, and returns the result to the browser as plain HTML.

ASP.NET Razor

	Razor is a new and simple markup syntax for embedding server code into ASP.NET web pages, much like Classic ASP.
	 
	Razor has the power of traditional ASP.NET, but is easier to use and easier to learn.

ASP.NET Programming Language
	
	C# 
	VB

ASP.NET Server Technologies

	Web Pages (with Razor syntax)
	MVC (Model View Controller)
	Web Forms (traditional ASP.NET)

ASP.NET Development Tools
	
	Web Matrix
	Visual Web Developer
	Visual Studio

ASP.NET File Extensions
	
	Classic ASP Files have the file extension .asp
	ASP.NET files have the file extension .aspx
	ASP.NET files with RAZOR C# Syntax have the file extension .cshtml
	ASP.NET files with RAZOR VB Syntax have the file extension .vbhtml

