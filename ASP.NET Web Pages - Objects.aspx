ASP.NET Web Pages - Objects

Web Page is much often objects


Some Page Object Methods
	
	# href = Builds a URL the specified parameter
	
	# RenderBody() - Renders the portion of a content page that is not within a named section [in Layout Page]

	# RenderPage(page) - Renders the content of the one page to another 

	# RenderSection(section) - Renders the content of a named section (In Layout Pages)

	# Write(object) - writes the object as an HTML-encoded string

	# WriteLiteral - Writes an object without HTML-encoding it first.

Some Page Object Properties
	
	isPost - Returns true if the HTTP data transfert method uses by the client is a POST Request
	
	Layout - Gets or sets the path of a layout page

	Page - Provides property-like access to data shared between pages and layout files

	Request - Gets the HttpRequest Object for the current HTTP Request

	Server - Gets the HttpServerUtility object that provides web-page processing methods.

The Page Property (of the Page Object)
	
	The Page property of the Page Object, provides property-like access to data shared between pages and layout pages.

You can use (add) your own properties to the Page property:

	Page.Title
	Page.Version
	Page.anythingyoulike
	
	The pages property is very helpful. For instance, it makes it possible to set the page title in content files, and use it in the layout file

@{
	Layout = "~/Shared/Layout.cshtml";
	Page.Title = "HomePage";
}

<h3>Welcome to W3Schools (Default.cshtml)</h3>
<p>A Layout File (Layout.cshtml)</p>
<p>A Style Sheet (Site.css)</p>


<!Doctype html>
<html>
	<head>
		<title>@Page.Title</title>
	</head>

	<body>
		@RenderBody()
	</body>
</html>
