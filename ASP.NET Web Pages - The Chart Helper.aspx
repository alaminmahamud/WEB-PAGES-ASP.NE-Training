ASP.NET Web Pages - The Chart Helper

	The Chart Helper - One of many useful ASP.NET Web Helpers.

The Chart Helper
	
	This chapter explains how to display data in graphical form, using the "Chart Helper".

	The "Chart Helper" can create chart images of different types with many formatting options and labels. It can create standard charts like area charts, bar charts, column charts, line charts, and pie charts, along with more specialized charts like stock charts.

	The Data you diplay in a chart can be from an array, from a database, or from data in a file

	@{
		var myChart = new Chart(width: 600, height: 400)
		.AddTitle("Employees")
		.AddSeries(chartType: "column", 
				   xValue: new[] {"Peter", "Andrew", "Mary", "Dave"},
				   yValue: new[] {"2", "4", "6", "8"})
				  )
		.Write();
	}

Chart From Database Data

	@{
		var db = Database.Open("Small Bakery");
		var dbdata = db.Query("SELECT Name, Price FROM Products");
		var myChart = new Chart(width:600 , height:400)
					  .AddTitle("Product Sales")
					  .DataBindTable(dataSource: dbdata, xField: "Name")
					  .Write();
	}

	- var db = Database.Open opens the database (and assigns the database object to the variable db)

- var dbdata = db.Query runs a database query and stores the result in dbdata

- new Chart creates a chart new object and sets its width and height

- the AddTitle method specifies the chart title

- the DataBindTable method binds the data source to the chart

- the Write() method displays the chart 

An alternative to using the DataBindTable method is to use AddSeries (See previous example). DataBindTable is easier to use, but AddSeries is more flexible because you can specify the chart and data more explicitly:

	@{
		var db = Database.Open("SmallBakery");	
		var dbdata = db.Query("SELECT Name, Price FROM Product");
		var myChart = new Chart(width: 600, height: 400)
					  .AddTitle("Product Sales")
					  .AddSeries(chartType: "Pie",
					  			 xValue: dbdata, xField: "Name",
					  			 yValues: dbdata, yFields: "Price")
					  .Write();
	}


Chart From XML Data

	The third option for charting is to use an XML file as the data for the chart:

	@using System.Data;

	@{
	var dataSet = new DataSet();
	dataSet.ReadXmlSchema(Server.MapPath("data.xsd"));
	dataSet.ReadXml(Server.MapPath("data.xml"));
	var dataView = new DataView(dataSet.Tables[0]);
	var myChart = new Chart(width: 600, height: 400)
	   .AddTitle("Sales Per Employee")
	   .AddSeries("Default", chartType: "Pie",
	      xValue: dataView, xField: "Name",
	      yValues: dataView, yFields: "Sales")
	   .Write();}
	}
