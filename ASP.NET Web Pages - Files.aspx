ASP.NET Web Pages - Files

Working With Text Files

	Text Files are used to store data is often called flat files. Common Text files are .txt, .xml, .csv (comma-delimited values).

Add A text file manually
	
	Folder Structure:

	Root/App_Data/Persons.txt

	File : Persons.txt

		George, Lucas
		Steven, SpeilBerg
		Alfred, Hitchcock

Displaying Data from a Text File
	
	@{
		var dataFile = Server.MapPath("~/App_Data/Persons.txt");
		Array userdata = File.ReadAllLines(dataFile);
	}

	<!Doctype html>
	<html>

	<body>
		@foreach(string dataLine in userData)
		{
			foreach(string dataItem in dataLine.Split(','))
			{
				@dataItem<text>&nbsp;</text>
			}
		}
	</body>

	</html>


Displaying data from an Excel File

	With Microsoft Excel, you can save a spreadsheet as a comma separeted text file (.csv file). When you do so, each row in the spreadsheet is saved as a text line, and each data column is separated by a comma.

	You can use the example above to read an Excel .csv file (just change the file name to the name of the Excel file).

