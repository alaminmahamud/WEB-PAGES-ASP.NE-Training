ASP.NET Web Pages - Database

Adding a Customer Page
	
	@{
		var db = Database.Open("SmallBakery");
		var selectQueryString = "SELECT * FROM Product ORDER BY Name";
	}

	<html> 
	<body> 
	<h1>Small Bakery Products</h1> 
	<table> 
	<tr>
	<th>Id</th> 
	<th>Product</th> 
	<th>Description</th> 
	<th>Price</th> 
	</tr>

	@foreach(var row in db.Query(selectQueryString))
	{
	<tr> 
	<td>@row.Id</td> 
	<td>@row.Name</td> 
	<td>@row.Description</td> 
	<td align="right">@row.Price</td> 
	</tr> 
	}
	</table> 
	</body> 
	</html>

Example Explained

	The Database.Open(name) method will connect to a database in two steps:

	First, it searches the application's App_Data folder for a database that 	matches the name parameter without the file-name extension.

	If no file is found, it looks for a "connection string" in the application's Web.config file.

	(A connection string contains information about how to connect to a database. It can include a file path, or the name of an SQL database, with full user name and password)

	This two-step search makes it possible to test the application with a local database, and run the application on a web host using a connection string.