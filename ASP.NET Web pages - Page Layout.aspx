With Web Pages it is easy to create a web site with a consistent layout.

A Consistent Look

	On the Internet you will discover many web sites with a consistent look and feel:

	Every page have the same header
	Every page have the same footer
	Every page have the same style and layout
	With Web Pages this can be done very efficiently. You can have reusable blocks of content (content blocks), like headers and footers, in separate files.

	You can also define a consistent layout for all your pages, using a layout template (layout file).

Content Blocks

	Many websites have content that is displayed on every page (like headers and footers).
	
	With Web Pages you can use the @RenderPage() method to import content from separate files.
	
	Content block (from another file) can be imported anywhere in a web page, and can contain text, markup, and code, just like any regular web page.
	
	Using common headers and footers as an example, this saves you a lot of work. You don't have to write the same content in every page, and when you change the header or footer files, the content is updated in all your pages.
	
	This is how it looks in code:	

	<html>
	<body>
		@RenderPage("header.cshtml");
		<h1>Hello Web Page </h1>
		<p>This is a Paragraph</p>
		@RenderPage("footer.cshtml");
	</body>
	</html>

Using a Layout Page
	
	Another approach to creating a consistent look is to use a layout page. A layout page contains the structure, but not the content, of a web page. When a web page (content page) is linked to a layout page, it will be displayed according to the layout page (template).

	The layout page is just like a normal web page, except from a call to the @RenderBody() method where the content page will be included.
	
	Each content page must start with a Layout directive.


Layout Page : 

	<html>
	<body>
		<p> This is a header Text</p>
		@RenderBody()
		<p> &copy; 2012 W3Schools. All Rights Reserved</p>
	</body>
	</html>


Any Web Page:

	@{Layout = "Layout.cshtml";}

	<h1>Welcome To W3Schools</h1>
	<p>Lorem Ipsum Doler Sit Amet</p>

D.R.Y - Don't Repeat Yourself

	with two ASP.NET tools, Content Blocks and Layout Pages, you can give your web application a consistent look.

	These tools also save you a lot of work, since you don't have to repeat the same information on all pages. Centralizing markup, style and code makes web applications much more negligible and easier to maintain.

Preventing Files from Being Browsed
	
	with ASP.NET, files with a name that starts with an underscore cannot be browsed from the web.

	If you want your content blocks or layout files from being viewed by your users, rename the files to:

		_header.cshtml,
		_footer.cshtml,
		_Layout.cshtml

Hiding Sensitive information:
	
	In ASP.NET, the common way to hide sensitive information(database passwords, email passwords, etc.) is to keep the information in a separate file named "_AppStart.cshtml"

	@{
		WebMail.SmtpSerer = "mailserver.example.com";
		WebMail.EnableSsl = true;
		WebMail.UserName = "username@example.com";
		WebMail.Password = "your-password";
		WebMail.From = "your-name-here@example.com";	
	}







