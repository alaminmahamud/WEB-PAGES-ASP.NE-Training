ASP.NET Web Pages - Folders

Logical Folder Structure

	Demo
		Account {logon and security details}
		App_data {database and data files}
		Images {images}
		Scripts {browser scripts}
		Shared {contain common files {layout and style files}}

URLS and Paths

	URL	http://www.w3schools.com/html/html5_intro.asp
	Server name	w3schools
	Virtual path	/html/html5_intro.asp
	Physical path	C:\MyWebSites\w3schools\html\html5_intro.asp
	The root on a disk drive is written like C:\, but the root on a web site is  / (forward slash).

The ~Operator

	To Specify the virtual root in programming code, use the ~operator

	var myImagesFolder = "~/images";
	var myStyleSheet = "~/styles/StyleSheet.cs";

The Server.MapPath Method

	The Server.MapPath method converts a virtual path (/default.cshtml) to a physical path that the server can understand (C:\Johnny\MyWebSited\Demo\default.cshtml).

	You will use this method when you need to open data files located on the server (data files can only be accessed with a full physical path):

	 var pathname = "~/datafile.txt";
	 var fileName = Server.MapPath(pathname);

The Href Method
	
	The Href method converts a path used in the code to a path that the browser can understand (the browser cannot understand the ~ operator).

	You use the Href method to create paths to resources like image files, and CSS files.

	You will often use this method in HTML <a>, <img>, and <link> elements:

	@{var myStyleSheet = "~/Shared/Site.css";}

	<!--This Creates a link to the CSS file -->
	<link rel = "stylesheet" type = "text/css" href="@Href(myStyleSheet)" />

	<!-- Same As : -->
	<link rel = "stylesheet" type = "text/css" href = "/Shared/Site.css"/>

	The Href method is a method of WebPage Object.


	
