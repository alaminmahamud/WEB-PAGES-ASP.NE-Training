ASP.NET Web Pages - The WebGrid Helper

	WebGrid - One of many useful ASP.NET Web Helpers.

	@{
	var db = Database.Open("SmallBakery"); 
	var selectQueryString = "SELECT * FROM Product ORDER BY Name"; 
	}
	<html> 
	<body> 
	<h1>Small Bakery Products</h1> 
	<table> 
	<tr>
	<th>Id</th> 
	<th>Product</th> 
	<th>Description</th> 
	<th>Price</th> 
	</tr>
	@foreach(var row in db.Query(selectQueryString))
	{
	<tr> 
	<td>@row.Id</td> 
	<td>@row.Name</td> 
	<td>@row.Description</td> 
	<td align="right">@row.Price</td> 
	</tr> 
	}
	</table> 
	</body> 
	</html>

Using the WebGrid Helper

	using the webgrid helper is an easier way to display the data

	The WebGrid Helper : 
		Automatically sets up an HTML table to display data
		Supports different options for formatting
		Supports paging through data
		Supports Sorting by clicking on column headings

	@{
		var db = Database.Open("Small Bakery");
		var selectQueryString = "SELECT * FROM Products ORDER BY Name";
		var data = db.Query(selectQueryString);
		var grid = new WebGrid(data);
	}
	<html> 
	<head> 
	<title>Displaying Data Using the WebGrid Helper</title> 
	</head> 
	<body> 
	<h1>Small Bakery Products</h1> 
	<div id="grid"> 
	@grid.GetHtml()
	</div> 
	</body> 
	</html>

