ASP.NET Web Pages - The WebMail Helper

Scenario: Email Support
	
	To demonstrate the use of email, we will create an input page for support, let the user submit the page to another page, and send an email about the support problem.

First: Edit Your AppStart Page

	 
	File : _AppStart.cshtml

	@{
		WebSecurity.InitializeDatabaseConnection(	
			"Users",
			"User Profile",
			"User Id",
			"Email",
			true
		);
	}

	To initialize the WebMail Helper, add the following WebMail Properties to your AppStart Page:

	File : _AppStart.cshtml
	@{
		WebSecurity.InitializeDatabaseConnection(	
			"Users",
			"User Profile",
			"User Id",
			"Email",
			true
		);

		WebMail.SmtpServer = "smtp@eaxmple.com";
		WebMail.SmtpPort = 25;
		WebMail.EnableSsl = false;
		WebMail.UserName = "support@example.com";
		WebMail.Password =  "Password - goes - here";
		WebMail.From	 = "john@example.com"; 
	}

	SmtpServer: The name the SMTP server that will be used to send the emails.

	SmtpPort: The port the server will use to send SMTP transactions (emails).

	EnableSsl: True, if the server should use SSL (Secure Socket Layer) encryption.

	UserName: The name of the SMTP email account used to send the email.

	Password: The password of the SMTP email account.

	From: The email to appear in the from address (often the same as UserName).


Second: Create an Email Input Page 
	
	Then create an input page, and name it Email_input

	File: Email_Input.cshtml
		
	<!DOCTYPE html> 
	<html> 
	<body> 
	<h1>Request for Assistance</h1> 

	<form method="post" action="EmailSend.cshtml"> 
	<label>Username:</label>
	<input type="text name="customerEmail" />
	<label>Details about the problem:</label> 
	<textarea name="customerRequest" cols="45" rows="4"></textarea> 
	<p><input type="submit" value="Submit" /></p> 
	</form> 

	</body> 
	</html>

The purpose of the input page is to collect information, then submit the data to a new page that can send the information as an email.



Third: Create An Email Send Page

	Then create the page that will be used to send the email, and name it Email_send:

	File: Email_Send.cshtml

	@{
		// Read Input 
		var customerEmail = Request["customerEmail"];
		var customerRequest = Request["customerRequest"];

		try
		{
			// send email
			WebMail.Send(
				to: "someone@example.com",
				subject: "Help Request from - " + customerEmail, 
				body: customerRequest
			);

		}
		catch(Exception ex)
		{
			<text>@ex</text>
		}
	}